# lnmp-docker-compose

### 背景：

- 频繁的配置运行环境，耗时耗力。
- 不同平台和系统下，会遇到各种奇奇怪怪的问题。
- 开发，测试，线上环境不一致问题。

---

### 介绍:

使用docker-compose搭建标准统一的lnmp运行环境。

包括：

- nginx
- mysql 
- php5
- php7

![](https://img.slimframework.net/imgs/2020/07/b6b28d7ae2fdf43b.png)

---

### 准备:

- Ubuntu (win上开发推荐：[vagrant-ubuntu-for-win](https://gitee.com/zxf.0810/vagrant-ubuntu-for-win))

- docker 17.04.0-ce (already installed)

- docker-compose 1.13.0 (already installed)

- Git

---

### 开始:

$ git clone https://gitee.com/zxf.0810/lnmp-docker-new.git

$ cd lnmp-docker-new

$ docker-compose up -d （稍等片刻，本地测试需要20分钟左右）

$ chown -R www-data.www-data app/*

$ chmod -R 700 app/*

---

### 运行：

- 配置本地hosts（ip地址为server的ip）

    192.168.1.1 www.hello.in

- 访问: http://hello.in 

---
### 最近更新

#### @2020/07/09

* 更新php7到php7.4

#### @2020/05/18

* add logs dir

#### @2019/12/02

* 更改PHP的docker安装源为国内镜像

#### @2018/10/24

* add php7.2 docker 
* add php7 to docker-compose

---
### ToDo List

- 更新docker-compose.yml到最新版本
- 添加详细使用手册

---
### 问题咨询

    微信：marlin-online
    QQ：3392175105  